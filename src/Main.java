import com.homework01.ConditionalStatements;
import com.homework01.Cycles;
import com.homework01.Function;
import com.homework01.MyArrays;

public class Main {

    public static void main(String[] args){

        ConditionalStatements condStait = new ConditionalStatements();
        Cycles cycleses = new Cycles();
        MyArrays myArrays = new MyArrays();
        Function fun = new Function();
///////////////-----------ConditionalStatement------------

        ////---01--- Если а четное--

        System.out.println(condStait.chekEven(16,58));

        /////---02---Точка на четверти

        System.out.println(condStait.defineQuarter(5,-5));

        /////---03---Сума положительных из трех чисел

       System.out.println("Сумма только положительных из трех чисел равна = " + condStait.getSumThreeNum(-8,-4,-9));

        /////---04---Посчитать выражение из трех чисел

        System.out.println("Результат - "+ condStait.countTheExpression(0,21,3));

        /////---05---Определение оценки по баллам

        System.out.println(condStait.defineGrade(30));

//////////////----------------com.homework01.Cycles---------------------

        /////---01---Сумма четных чисел от 0 до 99

        System.out.println(cycleses.getSumEven(100));

        ////---02---Проверить простое ли число

        System.out.println(cycleses.checkIsItAPrimeNumber(13));

        ////---03---Квадратный корень натурального числа ( метод последовательного подбора)

        System.out.println("Корень числа(метод последовательного подбора) - "+ cycleses.getSqrtNum(17));

        //////---binarry--

        System.out.println("Корень числа ( метод бинарного поиска) - "+ cycleses.getBinarrySqrt(17));

        //////---04---Факториал числа

        System.out.println("Факториал введенного числа - "+ cycleses.getfactorial(6));

        /////---05---Сума цифр заданого числа

        System.out.println(cycleses.getSumOfaGivenNumber(324123));

        ////---06---Зеркальное отображение числа

        System.out.println(cycleses.getMirrorReflection(23453234));

///////////////////////-----------Arrays-----------

        int[] myMultiArray = new int[] {1,55,45,-58,54,12,0,65,78,6,7};

        /////---01---Минимальний элеммент массива

        System.out.println("Минимальний элеммент массива "+myArrays.findMinElemArray(myMultiArray));

        ////---02---Максимальний элемент массива

        System.out.println("Максимальний элемент массива "+myArrays.findMaxElemArray(myMultiArray));

        ////---03---Индекс минимального элю массива

        System.out.println("Индекс мин. эле. массива "+ myArrays.minIndexElemArray(myMultiArray));

        //////---04---Индекс макс. эле. массива

        System.out.println("Индекс макс. эле. массива "+ myArrays.maxIndexElemArray(myMultiArray));

        /////---05---Сумма эле. массива с нечетными индексами

        System.out.println("Сумма эле. массива с нечетными индексами "+ myArrays.getSumOddNumbers(myMultiArray));

        ////---06---Реверс масива

        System.out.println("Реверс массива "+ myArrays.reversArr(myMultiArray));

        ////---07---Количество нечетных элементов массива

        System.out.println("Количество нечетных элементов массива - "+ myArrays.countNonEvenNumbers(myMultiArray));

        ////---08---Поменять местами первую и вторую половину массива

        System.out.println("Массив после замены половин - "+ myArrays.changeHalfArray(myMultiArray));

        ////---09---Сортировка массивов

        System.out.println("Сортировка пузирьком - "+ myArrays.sortBubble(myMultiArray));
        System.out.println("Сортировка выбором - "+ myArrays.sortSelect(myMultiArray));
        System.out.println("Сортировка вставками - "+ myArrays.sortInsert(myMultiArray));


//////////////////////----------------com.homework01.Function----------

        ////---01---Название дня недели по номеру

        System.out.println(fun.changeDayNumber(5));

        ////---02---Ростояние между точками

        System.out.println(fun.checkLengthBetweenPoints(5,-58,-2,6));

        ////---03---Число прописью

        fun.findWords(156);

        ////---04---Число цифрой

        fun.findNumberWords("twenty five");

    }
}