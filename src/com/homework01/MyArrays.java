package com.homework01;

import java.util.Arrays;

public class MyArrays {


    public int findMinElemArray(int[] myArray) {
        int min = myArray[0];
        for (int i = 0; i < myArray.length; i++) {
            if (min > myArray[i]) {
                min = myArray[i];
            }
        }
        return min;
    }


    public int findMaxElemArray(int[] myArray) {
        int max = myArray[0];
        for (int i = 0; i < myArray.length; i++) {
            if (max < myArray[i]) {
                max = myArray[i];
            }
        }
        return max;
    }


    public int minIndexElemArray(int[] myArray) {
        int min = myArray[0];
        int minInd = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (min > myArray[i]) {
                min = myArray[i];
                minInd = i;
            }
        }
        return minInd;
    }


    public int maxIndexElemArray(int[] myArray) {
        int max = myArray[0];
        int maxInd = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (max < myArray[i]) {
                max = myArray[i];
                maxInd = i;
            }
        }
        return maxInd;
    }


    public int getSumOddNumbers(int[] myArray) {
        int sum = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (i % 2 != 0) {
                sum = sum + myArray[i];
            }
        }
        return sum;
    }


    public String reversArr(int[] myArray) {
        int b = (myArray.length - 1);
        int[] reversMyArray = new int[myArray.length];
        for (int i = 0; i < myArray.length; i++) {
            reversMyArray[i] = myArray[b];
            b--;
        }
        return Arrays.toString(reversMyArray);
    }

    public int countNonEvenNumbers(int[] myArray) {
        int countNonEven = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] % 2 != 0) {
                countNonEven++;
            }
        }
        return countNonEven;
    }

    public String changeHalfArray(int[] myArray) {

        if (myArray.length % 2 != 0) {
            int mod = myArray.length % 2;
            for (int i = 0; i < myArray.length / 2; i++) {
                int temp = myArray[i];
                myArray[i] = myArray[myArray.length / 2 + i + mod];
                myArray[myArray.length / 2 + i + mod] = temp;
            }
        }
        if (myArray.length % 2 == 0) {
            int mod = ((myArray.length / 2));

            for (int i = 0; i < myArray.length / 2; i++) {
                int temp = myArray[i];
                myArray[i] = myArray[myArray.length / 2 + i];
                myArray[myArray.length / 2 + i] = temp;
            }
        }
        return Arrays.toString(myArray);
    }


    public String sortBubble(int[] myArray) {

        for (int i = myArray.length - 1; i >= 1; i--) {
            for (int in = 0; in < i; in++) {
                if (myArray[in] > myArray[in + 1]) {
                    int dummy = myArray[in];
                    myArray[in] = myArray[in + 1];
                    myArray[in + 1] = dummy;
                }
            }
        }
        return Arrays.toString(myArray);
    }


    public String sortSelect(int[] myArray) {

        for (int i = 0; i < myArray.length; i++) {
            int min = myArray[i];
            int imin = i;
            for (int j = i + 1; j < myArray.length; j++) {
                if (myArray[j] < min) {
                    min = myArray[j];
                    imin = j;
                }
            }
            if (i != imin) {
                int temp = myArray[i];
                myArray[i] = myArray[imin];
                myArray[imin] = temp;
            }
        }
        return Arrays.toString(myArray);
    }

    public String sortInsert(int[] myArray) {

        for (int left = 0; left < myArray.length; left++) {
            int value = myArray[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < myArray[i]) {
                    myArray[i + 1] = myArray[i];
                } else {
                    break;
                }
            }
            myArray[i + 1] = value;
        }
        return Arrays.toString(myArray);
    }
}







