package com.homework01;

public class Cycles {


    public String getSumEven(int num) {
        String result;
        int sum = 0;
        int counter = 0;

        for (int i = 1; i < num; i++) {
            if (i % 2 == 0) {
                sum = sum + i;
                counter = counter + 1;
            }
        }
        result ="Результат суммы " + sum + ",число четных чисел - " + counter;
        return result;
    }


    public String checkIsItAPrimeNumber(int num) {
        String prime;
        String noPrime;
        boolean isPrime = true;
        for (int p = 2; p < num; p++) {
            if (num % p == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            prime = "Число " + num + " является простым числом";
            return prime;
        } else {
           noPrime = "Число " + num + " не является простым числом";
            return noPrime;
        }
    }


    public int getSqrtNum(int num) {
        int sqrt = 0;
        for (int i = 1; i <= num; i++) {
            if ((i * i) == num) {
                sqrt = i;
            }
            if (i * i > num) {
                sqrt = i - 1;
                return sqrt;
            }

        }
        return sqrt;
    }


    public int getBinarrySqrt(int num) {
        int left = 0;
        int right = num;

        if (num < 2) {
            return num;
        }
        while (left < right) {
            int mid = left + ((right - left) / 2);
            if (mid * mid == num) {
                return mid;
            } else if (mid * mid < num) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return (left - 1);
    }


    public int getfactorial(int n) {
        int factorial = 1;
        for (int i = 1; i <= n; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }


    public int getSumOfaGivenNumber(int num) {
        int sum = 0;

        for (int i = num; i > 0; i = i / 10) {
            sum = sum + i % 10;
        }
        return sum;
    }


    public String getMirrorReflection(int n) {
        String mirNum = "";
        int num = 0;
        for (int i = n; i > 0; i = i / 10) {
            num = i % 10;
            mirNum += num;
        }
        return mirNum;
    }
}






