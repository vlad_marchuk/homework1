package com.homework01;

public class Function {


    //Название дня недели по номеру
    public String changeDayNumber(int numberDay) {

        String nameOfDay;

        String[] myArray = new String[]{"", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        if (1 <= numberDay && numberDay <= 7) {
            nameOfDay = myArray[numberDay];
            return nameOfDay;
        }
        return "Нету дня недели с таким номером";
    }


    //Ростояние между точками
    public double checkLengthBetweenPoints(double x1, double y1, double x2, double y2) {

        double sum = ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));

        double num;
        double lenght = sum / 2;
        do {
            num = lenght;
            lenght = (num + (sum / num)) / 2;
        } while ((num - lenght) != 0);
        return lenght;

    }


    public void findWords(int num) {
        int[] arrayBox = new int[]{-1, -1, -1};
        int thirdNumber = -1;
        int secondNumber = -1;
        int firstNumber = -1;
        int count = 0;

        for (int i = num; i > 0; i = i / 10) {
            arrayBox[count] = i % 10;
            count++;
        }
        thirdNumber = arrayBox[0];
        secondNumber = arrayBox[1];
        firstNumber = arrayBox[2];

        String[] arrayFirstTen = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        String[] arraySecondTen = new String[]{"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        String[] arrayDozens = new String[]{"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

        if (firstNumber < 0 & secondNumber < 0 & thirdNumber >= 0) {
            System.out.println(arrayFirstTen[thirdNumber]);
        }
        if (firstNumber < 0 & secondNumber == 1 & thirdNumber >= 0) {
            System.out.println(arraySecondTen[thirdNumber]);
        }
        if (firstNumber < 0 & secondNumber >= 2 & thirdNumber >= 0) {
            if (thirdNumber == 0) {
                System.out.println(arrayDozens[secondNumber]);
            } else
                System.out.println(arrayDozens[secondNumber] + " " + arrayFirstTen[thirdNumber]);
        }
        if (firstNumber > 0 & secondNumber >= 0 & thirdNumber >= 0) {
            if (secondNumber == 0 & thirdNumber == 0) {
                System.out.printf(arrayFirstTen[firstNumber] + " hundred ");
            }
            if (secondNumber == 0 & thirdNumber != 0) {
                System.out.printf(arrayFirstTen[firstNumber] + " hundred " + arrayFirstTen[thirdNumber]);
            }
            if (secondNumber != 0 & thirdNumber == 0) {
                System.out.println(arrayFirstTen[firstNumber] + " hundred " + arrayDozens[secondNumber]);
            }
            if (secondNumber != 0 & thirdNumber != 0) {
                System.out.println(arrayFirstTen[firstNumber] + " hundred " + arrayDozens[secondNumber] + " " + arrayFirstTen[thirdNumber]);
            }

        }
    }

    public void findNumberWords(String numberWritten) {

        String[] arrayFirstTen = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen",
                "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};

        String hundred = "hundred";
        int zero = 0;
        String[] words = numberWritten.split(" ");

        ///////////// Если в массиве 4 числа
        if (words.length == 4) {
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[0])) {
                    System.out.print(i);
                }
            }
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[2])) {
                    System.out.print(i / 10);
                }
            }
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[3])) {
                    System.out.print(i);
                }
            }
        }
        ///////////// Если в массиве 3 числа
        if (words.length == 3) {
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[0])) {
                    System.out.print(i);
                }
            }
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[2])) {
                    if (i > 9) {
                        System.out.print(i);
                    } else
                        System.out.print(zero);
                    System.out.print(i);
                }
            }
        }
        ///////////// Если в массиве 2 числа , одно из них слово хандред
        if (words.length == 2) {
            if (hundred.equals(words[1])) {
                for (int i = 0; i < arrayFirstTen.length; i++) {
                    if (arrayFirstTen[i].equals(words[0])) {
                        System.out.print(i);
                        System.out.print(zero);
                        System.out.print(zero);
                        break;

                    }
                }
            }
        }
        ///////////// Если в массиве 2 числа , без хандред
        if (words.length == 2) {
            if (!hundred.equals(words[1])) {
                for (int i = 0; i < arrayFirstTen.length; i++) {
                    if (arrayFirstTen[i].equals(words[0])) {
                        System.out.print(i / 10);
                    }
                }
                for (int i = 0; i < arrayFirstTen.length; i++) {
                    if (arrayFirstTen[i].equals(words[1])) {
                        System.out.print(i);
                    }
                }
            }
        }
        ///////////// Если в массиве 1 число
        if (words.length == 1) {
            for (int i = 0; i < arrayFirstTen.length; i++) {
                if (arrayFirstTen[i].equals(words[0])) {
                    System.out.print(i);
                }
            }
        }
    }
}



