package com.homework01;

public class ConditionalStatements {

    public String chekEven(int a, int b) {
        String result;

        if (a % 2 == 0) {
            result = "Результат переумножения " + a * b;
            return result;
        } else
            result = "Результат суммы " + (a + b);
            return  result;
    }

    public String defineQuarter(int x, int y) {
        String result;

        if (x < 0 && y > 0) {
            return result = "Координаты в четверти 1";
        }
        if (x > 0 && y > 0) {
            return result = "Координаты в четверти 2";

        }
        if (x < 0 && y < 0) {
            return result = "Координаты в четверти 3";

        }
        if (x > 0 && y < 0) {
            return result = "Координаты в четверти 4";

        } else
            return result = "Точка не принадлежат ни к одной из четверти координат";
    }


    public int getSumThreeNum(int firstNum, int secondNum, int thirdNum) {

        int sum = 0;

        if (firstNum > 0) {
            sum = sum + firstNum;
        }
        if (secondNum > 0) {
            sum = sum + secondNum;
        }
        if (thirdNum > 0) {
            sum = sum + thirdNum;
        }
        return sum;
    }


    public int countTheExpression(int firstNum, int secondNum, int thirdNum) {
        int result;

        if (firstNum + secondNum + thirdNum < firstNum * secondNum * thirdNum) {
            result = (firstNum * secondNum * thirdNum) + 3;
            return result;
        } else
            result = (firstNum + secondNum + thirdNum) + 3;
            return  result;
    }


    public String defineGrade (int grade){
        String strGrade;

        if (grade >= 0 & grade <= 19) {
             return strGrade = "Ваша оценка F, с результатом = " + grade;
        }
        if (grade >= 20 & grade <= 39) {
            return strGrade = "Ваша оценка E, с результатом = " + grade;
        }
        if (grade >= 40 & grade <= 59) {
            return strGrade = "Ваша оценка D, с результатом = " + grade;
        }
        if (grade >= 60 & grade <= 74) {
            return strGrade = "Ваша оценка C, с результатом = " + grade;
        }
        if (grade >= 75 & grade <= 89) {
            return strGrade = "Ваша оценка B, с результатом = " + grade;
        }
        if (grade >= 90 & grade <= 100) {
            return strGrade = "Ваша оценка A, с результатом = " + grade;
        }
        else
        return strGrade = "Вы не правильно ввели оценку, введите еще раз. ";
    }
}




