package test.com.homework01;

import com.homework01.MyArrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class MyArraysTest {

    public final MyArrays cut = new MyArrays();

    static Arguments[] findMinElemArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},-58 ),
                Arguments.arguments(new int[]{1,5,45,8,54,12,0,65,7,6,7},0 ),

        };
    }
    static Arguments[] findMaxElemArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},78 ),
                Arguments.arguments(new int[]{1,5,45,8,54,12,0,65,7,6,7},65 ),

        };
    }
    static Arguments[] minIndexElemArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},3 ),
                Arguments.arguments(new int[]{1,5,45,8,54,12,0,65,7,6,7},6 ),

        };
    }
    static Arguments[] maxIndexElemArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},8 ),
                Arguments.arguments(new int[]{1,5,45,8,54,12,0,65,7,6,7},7 ),

        };
    }
    static Arguments[]getSumOddNumbersTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},80 ),
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7,5,9,1,10},86 ),

        };
    }
    static Arguments[]reversArrTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},"[7, 6, 78, 65, 0, 12, 54, -58, 45, 55, 1]" ),
                Arguments.arguments(new int[]{1,4,5,6,9,8,4,2,10},"[10, 2, 4, 8, 9, 6, 5, 4, 1]" ),

        };
    }
    static Arguments[]countNonEvenNumbersTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},5 ),
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7,5,9,1,10},8 ),

        };
    }
    static Arguments[]changeHalfArrayTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},"[0, 65, 78, 6, 7, 12, 1, 55, 45, -58, 54]" ),
                Arguments.arguments(new int[]{1,5,-58,54,12,0,65,78},"[12, 0, 65, 78, 1, 5, -58, 54]" ),
        };
    }
    static Arguments[]sortBubbleTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},"[-58, 0, 1, 6, 7, 12, 45, 54, 55, 65, 78]" ),
                Arguments.arguments(new int[]{1,8,5,4,6,10,5},"[1, 4, 5, 5, 6, 8, 10]" ),

        };
    }
    static Arguments[]sortSelectTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},"[-58, 0, 1, 6, 7, 12, 45, 54, 55, 65, 78]" ),
                Arguments.arguments(new int[]{1,8,5,4,6,10,5},"[1, 4, 5, 5, 6, 8, 10]" ),

        };
    }
    static Arguments[]sortInsertTestArgs() {
        return new Arguments[]{
                Arguments.arguments(new int[]{1,55,45,-58,54,12,0,65,78,6,7},"[-58, 0, 1, 6, 7, 12, 45, 54, 55, 65, 78]" ),
                Arguments.arguments(new int[]{1,8,5,4,6,10,5},"[1, 4, 5, 5, 6, 8, 10]" ),

        };
    }



    @ParameterizedTest
    @MethodSource("findMinElemArrayTestArgs")
    void findMinElemArrayTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.findMinElemArray(num));
    }

    @ParameterizedTest
    @MethodSource("findMaxElemArrayTestArgs")
    void findMaxElemArrayTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.findMaxElemArray(num));
    }

    @ParameterizedTest
    @MethodSource("minIndexElemArrayTestArgs")
    void minIndexElemArrayTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.minIndexElemArray(num));
    }

    @ParameterizedTest
    @MethodSource("maxIndexElemArrayTestArgs")
    void  maxIndexElemArrayTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.maxIndexElemArray(num));
    }

    @ParameterizedTest
    @MethodSource("getSumOddNumbersTestArgs")
    void  getSumOddNumbersTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.getSumOddNumbers(num));
    }

    @ParameterizedTest
    @MethodSource("reversArrTestArgs")
    void  reversArrTest(int[] num, String expected ){
        Assertions.assertEquals(expected, cut.reversArr(num));
    }

    @ParameterizedTest
    @MethodSource("countNonEvenNumbersTestArgs")
    void  countNonEvenNumbersTest(int[] num, int expected ){
        Assertions.assertEquals(expected, cut.countNonEvenNumbers(num));
    }
    @ParameterizedTest
    @MethodSource("changeHalfArrayTestArgs")
    void  changeHalfArrayTest(int[] num, String expected ){
        Assertions.assertEquals(expected, cut.changeHalfArray(num));
    }
    @ParameterizedTest
    @MethodSource("sortBubbleTestArgs")
    void  sortBubbleTest(int[] num, String expected ){
        Assertions.assertEquals(expected, cut.sortBubble(num));
    }
    @ParameterizedTest
    @MethodSource("sortSelectTestArgs")
    void  sortSelect(int[] num, String expected ){
        Assertions.assertEquals(expected, cut.sortSelect(num));
    }
    @ParameterizedTest
    @MethodSource("sortInsertTestArgs")
    void  sortInsert(int[] num, String expected ){
        Assertions.assertEquals(expected, cut.sortInsert(num));
    }
}