package test.com.homework01;

import com.homework01.ConditionalStatements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ConditionalStatementsTest {

    public final ConditionalStatements cut = new ConditionalStatements();

    static Arguments[] chekEvenTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3,3,"Результат суммы 6"),
                Arguments.arguments(16,58,"Результат переумножения 928"),
        };
    }
    static Arguments[] defineQuarterTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5,-5,"Координаты в четверти 4"),
                Arguments.arguments(5,5,"Координаты в четверти 2"),
                Arguments.arguments(-9,9,"Координаты в четверти 1"),
                Arguments.arguments(-2,-5,"Координаты в четверти 3"),
                Arguments.arguments(0,0,"Точка не принадлежат ни к одной из четверти координат"),
        };
    }
    static Arguments[] getSumThreeNumTestArgs() {
        return new Arguments[]{
                Arguments.arguments(5, 4, 9, 18),
                Arguments.arguments(-8, 4, 9, 13),
                Arguments.arguments(-8, -4, 9, 9),
                Arguments.arguments(-8, -4, -9, 0),
        };
    }
    static Arguments[] countTheExpressionArgs() {
        return new Arguments[]{
                Arguments.arguments(41, 5, 7, 1438),
                Arguments.arguments(0, 21, 3, 27),
        };
    }
    static Arguments[] defineGradeTestArgs() {
        return new Arguments[]{
                Arguments.arguments(10,"Ваша оценка F, с результатом = 10"),
                Arguments.arguments(30,"Ваша оценка E, с результатом = 30"),
                Arguments.arguments(50,"Ваша оценка D, с результатом = 50"),
                Arguments.arguments(72,"Ваша оценка C, с результатом = 72"),
                Arguments.arguments(82,"Ваша оценка B, с результатом = 82"),
                Arguments.arguments(96,"Ваша оценка A, с результатом = 96"),
        };
    }

    @ParameterizedTest
    @MethodSource("chekEvenTestArgs")
    void chekEvenTest(int a , int b, String expected){
        Assertions.assertEquals(expected, cut.chekEven(a,b));
    }

    @ParameterizedTest
    @MethodSource("defineQuarterTestArgs")
    void defineQuarterTest(int a , int b, String expected){
        Assertions.assertEquals(expected, cut.defineQuarter(a,b));
    }

    @ParameterizedTest
    @MethodSource("getSumThreeNumTestArgs")
    void getSumThreeNumTest(int firstNum, int secondNum, int thirdNum, int expected ){
        Assertions.assertEquals(expected, cut.getSumThreeNum(firstNum,secondNum,thirdNum));
    }

    @ParameterizedTest
    @MethodSource("countTheExpressionArgs")
    void countTheExpressionTest(int firstNum, int secondNum, int thirdNum, int expected ){
        Assertions.assertEquals(expected, cut.countTheExpression(firstNum,secondNum,thirdNum));
    }
    @ParameterizedTest
    @MethodSource("defineGradeTestArgs")
    void defineGradeTest(int grade, String expected ){
        Assertions.assertEquals(expected, cut.defineGrade(grade));
    }
}