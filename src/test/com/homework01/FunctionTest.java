package test.com.homework01;

import com.homework01.Function;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class FunctionTest {

    Function cut = new Function();

    static Arguments[] changeDayNumberTestArgs(){
        return  new Arguments[]{
                Arguments.arguments(1,"Monday"),
                Arguments.arguments(5,"Friday"),
                Arguments.arguments(10,"Нету дня недели с таким номером"),
        };
    }
    static Arguments[] checkLengthBetweenPointsTestArgs(){
        return  new Arguments[]{
                Arguments.arguments(-5,0,5,0, 10),
                Arguments.arguments(5,-58,-2,6,64.38167441127949),

        };
    }

    @ParameterizedTest
    @MethodSource("changeDayNumberTestArgs")
    void  changeDayNumberTest(int num, String expected ){
        Assertions.assertEquals(expected, cut. changeDayNumber(num));
    }
    @ParameterizedTest
    @MethodSource("checkLengthBetweenPointsTestArgs")
    void  checkLengthBetweenPointsTest(double x1, double y1, double x2, double y2, double expected ){
        Assertions.assertEquals(expected, cut.checkLengthBetweenPoints(x1, y1, x2, y2));
    }



}
