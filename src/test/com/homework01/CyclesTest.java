package test.com.homework01;

import com.homework01.Cycles;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class CyclesTest {

    public final Cycles cut = new Cycles();

    static Arguments[] checkIsItAPrimeNumberTestArgs() {
        return new Arguments[]{
                Arguments.arguments(7,  "Число 7 является простым числом"),
                Arguments.arguments(4,  "Число 4 не является простым числом"),
        };
    }
    static Arguments[] getSqrtNumTestArgs() {
        return new Arguments[]{
                Arguments.arguments(16, 4 ),
                Arguments.arguments(4, 2),
        };
    }
    static Arguments[] getBinarrySqrtTestArgs() {
        return new Arguments[]{
                Arguments.arguments(16, 4 ),
                Arguments.arguments(25, 5),
        };
    }
    static Arguments[] getfactorialTestArgs() {
        return new Arguments[]{
                Arguments.arguments(2, 2 ),
                Arguments.arguments(10, 3628800),
        };
    }
    static Arguments[] getSumOfaGivenNumberTestArgs() {
        return new Arguments[]{
                Arguments.arguments(123, 6 ),
                Arguments.arguments(5254, 16),
        };
    }
    static Arguments[] getMirrorReflectionTestArgs() {
        return new Arguments[]{
                Arguments.arguments(151648, "846151" ),
                Arguments.arguments(5254, "4525"),
        };
    }


    @Test
    void getSumEvenTest() {
        String actual = cut.getSumEven(100);
        String expected = "Результат суммы 2450,число четных чисел - 49";
        assertEquals(actual, expected);
    }
    @ParameterizedTest
    @MethodSource("checkIsItAPrimeNumberTestArgs")
    void checkIsItAPrimeNumberTest(int num, String expected ){
        Assertions.assertEquals(expected, cut.checkIsItAPrimeNumber(num));
    }
    @ParameterizedTest
    @MethodSource("getSqrtNumTestArgs")
    void getSqrtNumTest(int num, int expected ){
        Assertions.assertEquals(expected, cut.getSqrtNum(num));
    }
    @ParameterizedTest
    @MethodSource("getBinarrySqrtTestArgs")
    void getBinarrySqrtTest(int num, int expected ){
        Assertions.assertEquals(expected, cut.getBinarrySqrt(num));
    }
    @ParameterizedTest
    @MethodSource("getfactorialTestArgs")
    void getfactorialTest(int num, int expected ){
        Assertions.assertEquals(expected, cut.getfactorial(num));
    }
    @ParameterizedTest
    @MethodSource("getSumOfaGivenNumberTestArgs")
    void getSumOfaGivenNumberTest(int num, int expected ){
        Assertions.assertEquals(expected, cut.getSumOfaGivenNumber(num));
    }

    @ParameterizedTest
    @MethodSource("getMirrorReflectionTestArgs")
    void getMirrorReflectionTest(int num, String expected ){
        Assertions.assertEquals(expected, cut.getMirrorReflection(num));
    }
}